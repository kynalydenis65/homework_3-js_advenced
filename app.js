// Завдання 1
// Дві компанії вирішили об'єднатись, і для цього їм потрібно об'єднати базу даних своїх клієнтів.
// 	У вас є 2 масиви рядків, у кожному з них – прізвища клієнтів. Створіть на
// їх основі один масив, який буде об'єднання ' +
// 'двох масивів без повторюваних прізвищ клієнтів.


const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

// Об'єднуємо два масиви і створюємо Set для видалення дублікатів
const mergedClientsSet = new Set([...clients1, ...clients2]);

// Перетворюємо Set назад у масив
const mergedClients = Array.from(mergedClientsSet);
console.log(mergedClients);


///////////////////////////////////////////////////////
const characters = [{
	name: "Елена", lastName: "Гилберт", age: 17, gender: "woman", status: "human"
}, {
	name: "Кэролайн", lastName: "Форбс", age: 17, gender: "woman", status: "human"
}, {
	name: "Аларик", lastName: "Зальцман", age: 31, gender: "man", status: "human"
}, {
	name: "Дэймон", lastName: "Сальваторе", age: 156, gender: "man", status: "vampire"
}, {
	name: "Ребекка", lastName: "Майклсон", age: 1089, gender: "woman", status: "vampire"
}, {
	name: "Клаус", lastName: "Майклсон", age: 1093, gender: "man", status: "vampire"
}];

const charactersShortInfo = characters.map(character => {
	return {
		name: character.name, lastName: character.lastName, age: character.age
	};
});

console.log(charactersShortInfo);


const user1 = {
	name: "John",
	years: 30
};

const {name: імʼя, years: вік, isAdmin = false} = user1;

console.log(імʼя); // виведе "John"
console.log(вік); // виведе 30
console.log(isAdmin); // виведе false


const satoshi2020 = {
	name: 'Nick',
	surname: 'Sabo',
	age: 51,
	country: 'Japan',
	birth: '1979-08-21',
	location: {
		lat: 38.869422,
		lng: 139.876632
	}
};

const satoshi2019 = {
	name: 'Dorian',
	surname: 'Nakamoto',
	age: 44,
	hidden: true,
	country: 'USA',
	wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
	browser: 'Chrome'
};

const satoshi2018 = {
	name: 'Satoshi',
	surname: 'Nakamoto',
	technology: 'Bitcoin',
	country: 'Japan',
	browser: 'Tor',
	birth: '1975-04-05'
};

const fullProfile = {};

// Об'єднуємо дані з об'єктів, перевіряючи наявність кожного поля
Object.assign(fullProfile, satoshi2018);
Object.assign(fullProfile, satoshi2019);
Object.assign(fullProfile, satoshi2020);

console.log(fullProfile);


const books = [{
	name: 'Harry Potter',
	author: 'J.K. Rowling'
}, {
	name: 'Lord of the rings',
	author: 'J.R.R. Tolkien'
}, {
	name: 'The witcher',
	author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
	name: 'Game of thrones',
	author: 'George R. R. Martin'
};

const newBooks = books.concat(bookToAdd);

console.log(newBooks);


const employee = {
	name: 'Vitalii',
	surname: 'Klichko'
};

// Створюємо новий об'єкт з усіма властивостями об'єкта employee та доданими властивостями age і salary
const newEmployee = {
	...employee,
	age: 30,
	salary: 50000
};

console.log(newEmployee);


const array = ['value', () => 'showValue'];

const [value, showValue] = array;

alert(value); // виведе 'value'
alert(showValue()); // виведе 'showValue'
